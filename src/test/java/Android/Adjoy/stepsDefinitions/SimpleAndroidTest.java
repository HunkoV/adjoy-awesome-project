package Android.Adjoy.stepsDefinitions;


import java.io.IOException;
import java.util.concurrent.TimeUnit;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.testng.AssertJUnit;

public class SimpleAndroidTest {
	@SuppressWarnings("rawtypes")
	public AndroidDriver driver;
	//constructor 
	public SimpleAndroidTest() throws IOException { 
    	driver = hooks.driver;	
	}

	@Given("^User stats from passing Tutorials$")
	public void user_stats_from_passing_Tutorials() throws Throwable {
	  //Get the size of screen.
	  Dimension size = driver.manage().window().getSize();
	  //Find start x point which is at right side of screen.
	  int startx = (int) (size.width * 0.75);
	  //Find end x point which is at left side of screen.
	  int endx = (int) (size.width * 0.25);
	  //Find vertical point where you wants to swipe. It is in middle of screen height.
	  int starty = size.height / 2;
	  
	  //Swipe from Right to Left to pass the first screen
	  driver.swipe(startx, starty, endx, starty, 800);
	  Thread.sleep(2000);
	  //Swipe from Right to Left to pass second screen
	  driver.swipe(startx, starty, endx, starty, 800);
	  Thread.sleep(2000);
	}

	@When("^User links his card with \"([^\"]*)\"$")
	public void user_links_his_card_with(String arg1) throws Throwable {
	//click on Start button
	driver.findElement(By.id("com.mynfo.marshfreshlettuce:id/buttonLetsGetStarted")).click();
	//enter number of card
	driver.findElement(By.id("com.mynfo.marshfreshlettuce:id/barcode_editText")).sendKeys(arg1);
	//click Link 
	driver.findElement(By.id("com.mynfo.marshfreshlettuce:id/button_link_card")).click();
	}

	@Then("^User sees the Main Menu screen$")
	public void user_sees_the_Main_Menu_screen() throws Throwable {
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	String header = driver.findElement(By.xpath("//android.widget.TextView[@text='Earn Fresh Lettuce']")).getText();
	AssertJUnit.assertEquals("Earn Fresh Lettuce",header);	
	Thread.sleep(3000);
	System.out.println("Done!");
}


}
