package Android.Adjoy.stepsDefinitions;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.appium.java_client.android.AndroidDriver;

public class hooks {
	  @SuppressWarnings("rawtypes")
	public static AndroidDriver driver;

	    
	    @SuppressWarnings("rawtypes")
		@Before
	    public void setUp() throws  MalformedURLException { 
	    	  // Created object of DesiredCapabilities class.
	    	  DesiredCapabilities capabilities = new DesiredCapabilities();
	    	  // Set android deviceName desired capability.
	    	  // !!!!!!!!!!!!!!!Set your device name.!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	    	  capabilities.setCapability("deviceName", "990c1a48665700000000)");    // "emulator-5554");
	    	  // Set BROWSER_NAME desired capability. It's Android in our case here.Do NOT touch it.
	    	  capabilities.setCapability(CapabilityType.BROWSER_NAME, "Android");
	    	  // Set android VERSION desired capability. Set your mobile device's OS version.
	    	  capabilities.setCapability(CapabilityType.VERSION, "4.4.2");          //5.1.1");
	    	  // Set android platformName desired capability. It's Android in our case here. Do Not touch it.
	    	  capabilities.setCapability("platformName", "Android");
	    	  // Set android appPackage desired capability. It is
	    	  //"com.android.calculator2"); - as example.
	    	  capabilities.setCapability("appPackage", "com.mynfo.marshfreshlettuce");
	    	  // Set android appActivity desired capability. It is
	    	  // com.android.calculator2.Calculator"); for calculator application.
	    	  capabilities.setCapability("appActivity", "com.mynfo.marshfreshlettuce.TutorialActivity");//SplashActivity");????
	    	  // Created object of RemoteWebDriver will all set capabilities.
	    	  // Set appium server address and port number in URL string.
	    	  // It will launch the app in android device.
	    	  driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
	    	  driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	    	  
	    }
	    
	    @After
	    /**
	     * Embed a screenshot in test report if test is marked as failed
	     */
	    public void embedScreenshot(Scenario scenario) {
	    if(scenario.isFailed()){
	      String destDir;
	   	  DateFormat dateFormat;
	   	  // Set folder name to store screenshots.
	   	  destDir = "screenshots";
	   	  // Capture screenshot.
	   	  File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
	   	  // Set date format to set It as screenshot file name.
	   	  dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
	   	  // Create folder under project with name "screenshots" provided to destDir.
	   	  new File(destDir).mkdirs();
	   	  // Set file name using current date time.
	   	  String destFile = dateFormat.format(new Date()) + ".png";

	   	  try {
	   	   // Copy paste file at destination folder location
	   	   FileUtils.copyFile(scrFile, new File(destDir + "/" + destFile));
	   	   } catch (IOException e) {
	   	   e.printStackTrace();
	   	   }
	    }
	      
	        driver.quit();
	        
	    }

}
