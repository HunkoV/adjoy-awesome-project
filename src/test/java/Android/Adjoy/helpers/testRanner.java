package Android.Adjoy.helpers;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = {"src/test/features/"},
		// this line works for mac os
		plugin = {"pretty", "html:target/cucumber-html-report","json:cucumber.json"},
		glue = {"Android.Adjoy.stepsDefinitions"},
		tags = {"@tag"},
		//dryRun = true,
		monochrome = false
		)

public class testRanner {

}
